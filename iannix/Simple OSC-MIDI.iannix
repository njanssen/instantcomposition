/*
 *	IanniX Score File
 */


/*
 *	This method is called first.
 *	It is the good section for asking user for script global variables (parameters).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function askUserForParameters() {
	//title("The title of the parameter box");
	//ask("Group name of the parameter (only for display purposes)", "Parameter label", "myGlobalVar", "theDefaultValue");
}


/*
 *	This method stores all the operations made through IanniX scripts.
 *	You can add some commands here to make your own scripts!
 *	Scripts are written in Javascript but even with a limited knowledge of Javascript, many types of useful scripts can be created.
 *	
 *	Beyond the standard javascript commands, the run() function is used to send commands to IanniX.
 *	Commands must be provided to run() as a single string.
 *	For example, run("zoom 100"); sets the display zoom to 100%.
 *	
 *	To combine numeric parameters with text commands to produce a string, use the concatenation operator.
 *	In the following example center_x and center_y are in numeric variables and must be concatenated to the command string.
 *	Example: run("setPos current " + center_x + " " + center_y + " 0");
 *	
 *	To learn IanniX commands, perform an manipulation in IanniX graphical user interface, and see the Helper window.
 *	You'll see the syntax of the command-equivalent action.
 *	
 *	And finally, remember that most of commands must target an object.
 *	Global syntax is always run("<command name> <target> <arguments>");
 *	Targets can be an ID (number) or a Group ID (string name of group) (please see "Info" tab in Inspector panel).
 *	Special targets are "current" (last used ID), "all" (all the objects) and "lastCurve" (last used curve).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function makeWithScript() {
	//Clears the score
	run("clear");
	//Resets rotation
	run("rotate 0 0 0");
	//Resets score viewport center
	run("center 0 0");
	//Resets score zoom
	run("zoom 100");
}


/*
 *	When an incoming message is received, this method is called.
 *		- <protocol> tells information about the nature of message ("osc", "midi", "direct…)
 *		- <host> and <port> gives the origin of message, specially for IP protocols (for OpenSoundControl, UDP or TCP, it is the IP and port of the application that sends the message)
 *		- <destination> is the supposed destination of message (for OpenSoundControl it is the path, for MIDI it is Control Change or Note on/off…)
 *		- <values> are an array of arguments contained in the message
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function onIncomingMessage(protocol, host, port, destination, values) {
	//Logs a message in the console (open "Config" tab from Inspector panel and see "Message log")
	console("Received on '" + protocol + "' (" + host + ":" + port + ") to '" + destination + "', " + values.length + " values : ");
	
	//Browses all the arguments and displays them in log window
	for(var valueIndex = 0 ; valueIndex < values.length ; valueIndex++)
		console("- arg " + valueIndex + " = " + values[valueIndex]);

	if (destination == "/accxyz" && values.length == 3) {
		var x = values[0];
		var y = values[1];
		var z = values[2];
		var cmd = "setpos 1 " + x + " " + y + " 0";
		console("Created command: " + cmd);
		run(cmd);
	}
}


/*
 *	This method stores all the operations made through the graphical user interface.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughGUI() {
//GUI: NEVER EVER REMOVE THIS LINE
	run("center 0.181242 0.0171103");
	run("zoom 100");
	run("rotate 0 0 0");


	run("add curve 1");
	run("setpos current -0.0888824 -0.164017 0");
	run("setpointsellipse current 2 2");
	run("add cursor 2");
	run("setcurve current lastCurve");
	run("setpos current -1.84351 -1.12384 0");
	run("setpattern current 0 0 1");
	run("settime current 1791.7089233398438");


	run("add trigger 15");
	run("setpos current 0.527909 0.543301 0");

	run("add trigger 12");
	run("setpos current -3.174 -0.583366 0");

	run("add trigger 13");
	run("setpos current -4.06542 0.741396 0");

	run("add trigger 10");
	run("setpos current 2.40981 -1.19003 0");

	run("add trigger 11");
	run("setpos current -1.81209 -2.84908 0");

	run("add trigger 8");
	run("setpos current -2.54257 2.63568 0");

	run("add trigger 9");
	run("setpos current 2.88029 2.21473 0");

	run("add trigger 6");
	run("setpos current 1.97648 -0.038604 0");

	run("add trigger 7");
	run("setpos current 0.0574325 -1.94527 0");

	run("add trigger 4");
	run("setpos current -1.96066 0.0233008 0");

	run("add trigger 5");
	run("setpos current -0.0044723 2.02902 0");

	run("add trigger 3");
	run("setpos current 0.0202896 -0.0138421 0");

	run("add trigger 16");
	run("setpos current 2.21172 1.36044 0");

	run("add trigger 17");
	run("setpos current -5.37781 2.38806 0");

	run("add trigger 14");
	run("setpos current -0.772091 0.691872 0");



//GUI: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method stores all the operations made by other softwares through one of the IanniX interfaces.
 *	You are not supposed to modify this section, but it can be useful to remove some stuff that you or a third party software added accidentaly.
 *	
 * 	Be very careful! This section is automaticaly overwritten when saving a score.
 */
function madeThroughInterfaces() {
//INTERFACES: NEVER EVER REMOVE THIS LINE

//INTERFACES: NEVER EVER REMOVE THIS LINE
}


/*
 *	This method is called last.
 *	It allows you to modify your hand-drawn score (made through graphical user interface).
 *	
 * 	This section is never overwritten by IanniX when saving.
 */
function alterateWithScript() {
	
}


/*
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 *	Made with IanniX 0.9.20
 *	//APP VERSION: NEVER EVER REMOVE THIS LINE
 */



/*
    This file is part of IanniX, a graphical real-time open-source sequencer for digital art
    Copyright (C) 2010-2015 — IanniX Association

    Project Manager: Thierry Coduys (http://www.le-hub.org)
    Development:     Guillaume Jacquemin (https://www.buzzinglight.com)

    This file was written by Guillaume Jacquemin.

    IanniX is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

