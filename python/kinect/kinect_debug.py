'''
Python code for interacive installation Instant Composition

Kinect debugger script

Requirements: 
- Microsoft Kinect v2 hardware 
- Microsoft Windows / Microsoft Kinect SDK v2 (https://www.microsoft.com/en-us/download/details.aspx?id=44561)
- Python 3.7
- PyKinect2 Python library (https://github.com/Kinect/PyKinect2)

-- Niels Janssen
'''
from pykinect2 import PyKinectV2
from pykinect2.PyKinectV2 import *
from pykinect2 import PyKinectRuntime
import numpy as np
import pandas as pd

def debug_kinect_body_frame_data(frame):
  debug_kinect_body_frame_floor_clip_plane(frame)

  for i in range(0, kinect.max_body_count):
    body = frame.bodies[i]
    if not body.is_tracked:
      continue
    debug_kinect_body_data(body)
    #debug_kinect_body_data_to_array(body)

def debug_kinect_body_frame_floor_clip_plane(frame):
  floor_clip_plane = frame.floor_clip_plane
  x = floor_clip_plane.x
  y = floor_clip_plane.y
  z = floor_clip_plane.z
  w = floor_clip_plane.w

  position = (x,y,z,w)  
  print('Floor clip plane: ',position)    

def debug_kinect_body_data_to_csv(body):

  columns = [
    # 'clip',
    'date',    
    # 'frame',
    # 'Floor_x',
    # 'Floor_y',
    # 'Floor_z',
    # 'Floor_w',            
    'Head_x',
    'Head_y',
    'Head_z',
    'Neck_x',
    'Neck_y',
    'Neck_z',
    # 'SpineShoulder_x',
    # 'SpineShoulder_y',
    # 'SpineShoulder_z',
    # 'SpineMid_x',
    # 'SpineMid_y',
    # 'SpineMid_z',
    # 'SpineBase_x',
    # 'SpineBase_y',
    # 'SpineBase_z',
    # 'ShoulderRight_x',
    # 'ShoulderRight_y',
    # 'ShoulderRight_z',
    # 'ShoulderLeft_x',
    # 'ShoulderLeft_y',
    # 'ShoulderLeft_z',
    # 'HipRight_x',
    # 'HipRight_y',
    # 'HipRight_z',
    # 'HipLeft_x',
    # 'HipLeft_y',
    # 'HipLeft_z',
    # 'ElbowRight_x',
    # 'ElbowRight_y',
    # 'ElbowRight_z',
    # 'WristRight_x',
    # 'WristRight_y',
    # 'WristRight_z',
    # 'HandRight_x',
    # 'HandRight_y',
    # 'HandRight_z',
    # 'HandTipRight_x',
    # 'HandTipRight_y',
    # 'HandTipRight_z',
    # 'ThumbRight_x',
    # 'ThumbRight_y',
    # 'ThumbRight_z',
    # 'ElbowLeft_x',
    # 'ElbowLeft_y',
    # 'ElbowLeft_z',
    # 'WristLeft_x',
    # 'WristLeft_y',
    # 'WristLeft_z',
    # 'HandLeft_x',
    # 'HandLeft_y',
    # 'HandLeft_z',
    # 'HandTipLeft_x',
    # 'HandTipLeft_y',
    # 'HandTipLeft_z',
    # 'ThumbLeft_x',
    # 'ThumbLeft_y',
    # 'ThumbLeft_z',    
    # 'KneeRight_x',
    # 'KneeRight_y',
    # 'KneeRight_z',
    # 'AnkleRight_x',
    # 'AnkleRight_y',
    # 'AnkleRight_z',
    # 'FootRight_x',
    # 'FootRight_y',
    # 'FootRight_z',
    # 'KneeLeft_x',
    # 'KneeLeft_y',
    # 'KneeLeft_z',
    # 'AnkleLeft_x',
    # 'AnkleLeft_y',
    # 'AnkleLeft_z',
    # 'FootLeft_x'
    # 'FootLeft_y'
    # 'FootLeft_z'
  ]

  df = pd.DataFrame(data,columns = columns)

def debug_kinect_append_joint_data_to_arr(join,arr):
  

def debug_kinect_body_data(body):
  joints = body.joints
  debug_kinect_joint_data(joints[PyKinectV2.JointType_Head],'Head')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_Neck],'Neck')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_SpineShoulder],'SpineShoulder')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_SpineMid],'SpineMid')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_SpineBase],'SpineBase')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_ShoulderRight],'ShoulderRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_ShoulderLeft],'ShoulderLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_HipRight],'HipRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_HipLeft],'HipLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_ElbowRight],'ElbowRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_WristRight],'WristRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_HandRight],'HandRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_HandTipRight],'HandTipRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_ThumbRight],'ThumbRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_ElbowLeft],'ElbowLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_WristLeft],'WristLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_HandLeft],'HandLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_HandTipLeft],'HandTipLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_ThumbLeft],'ThumbLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_KneeRight],'KneeRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_AnkleRight],'AnkleRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_FootRight],'FootRight')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_KneeLeft],'KneeLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_AnkleLeft],'AnkleLeft')
  debug_kinect_joint_data(joints[PyKinectV2.JointType_FootLeft],'FootLeft')

def debug_kinect_joint_data(joint,joint_name):
  x = joint.Position.x
  y = joint.Position.y
  z = joint.Position.z

  position = (x,y,z)  
  print('PyKinectV2.JointType_' + joint_name + ': ',position)  

kinect = PyKinectRuntime.PyKinectRuntime(
  PyKinectV2.FrameSourceTypes_Body | 
  PyKinectV2.FrameSourceTypes_Color |
  PyKinectV2.FrameSourceTypes_Infrared
)

Kinect_Color_Frame_Width = kinect.color_frame_desc.Width
Kinect_Color_Frame_Height = kinect.color_frame_desc.Height
Kinect_Infrared_Frame_Width = 512
Kinect_Infrared_Frame_Height = 424

while(True):
  if kinect.has_new_body_frame(): 
  #  print('kinect.has_new_body_frame')
    frame = kinect.get_last_body_frame()
    debug_kinect_body_frame_data(frame)

  #if kinect.has_new_color_frame():
  #  print('kinect.has_new_color_frame')
  #  frame = kinect.get_last_color_frame()

  #if kinect.has_new_infrared_frame():
  #  print('kinect.has_new_infrared_frame')
  #  frame = kinect.get_last_infrared_frame()
