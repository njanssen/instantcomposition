#!/usr/bin/python

# Can enable debug output for SI1145 by uncommenting:
#import logging
# logging.basicConfig(level=logging.DEBUG)

import time
import SI1145.SI1145 as SI1145

sensor = SI1145.SI1145()

while True:
    # returns visible + IR light levels
    vis = sensor.readVisible()

    # returns IR light levels
    IR = sensor.readIR()

    # returns the UV index * 100 (divide by 100 to get the index)
    UV = sensor.readUV()
    uvIndex = UV / 100.0

    print 'Vis:             ' + str(vis)
    print 'IR:              ' + str(IR)
    print 'UV Index:        ' + str(uvIndex)

    time.sleep(3)

/*
    import OSC
c = OSC.OSCClient()
c.connect(('127.0.0.1', 57120))   # connect to SuperCollider
oscmsg = OSC.OSCMessage()
oscmsg.setAddress("/startup")
oscmsg.append('HELLO')
c.send(oscmsg)
*/
