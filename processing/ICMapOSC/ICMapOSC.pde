import netP5.*;
import oscP5.*;

// Define constants for TidalCycles OSC Controller Input (see https://tidalcycles.org/index.php/Controller_Input)
static String TIDAL_CYCLES_OSC_CONTROLLER_INPUT="/ctrl";
static String TIDAL_CYCLES_ADDRESS="127.0.0.1";
static int TIDAL_CYCLES_PORT=6010;

OscP5 osc;
NetAddress tidalCyclesAddress;

void setup() { 
  size(400, 400);
  frameRate(25);  

  osc = new OscP5(this, 9000);
  tidalCyclesAddress = new NetAddress(TIDAL_CYCLES_ADDRESS,TIDAL_CYCLES_PORT);
}

void draw() {
  background(0);   
}

OscMessage oscTidalCyclesFloatMessage(String label, float value) {
  OscMessage oscMessage = new OscMessage(TIDAL_CYCLES_OSC_CONTROLLER_INPUT);
  oscMessage.add(label);
  oscMessage.add(value);
  return oscMessage;
}

void oscEvent(OscMessage oscMessage) {
  switch(oscMessage.addrPattern()) {
    case "/accxyz":
      // TouchOSC accelerometer data
      println("Mapping TouchOSC accelerometer data (" + oscMessage.addrPattern() + " " + oscMessage.typetag() + ") to TidalCycles controller input"); 
      if(oscMessage.checkTypetag("fff")) {
        // Message contains float values for x,y,z
        float x = oscMessage.get(0).floatValue();
        float y = oscMessage.get(1).floatValue();
        float z = oscMessage.get(2).floatValue();    
        println("Values: " + x + " | " + y + " | " + z);
        OscBundle bundle = new OscBundle();
        bundle.add(oscTidalCyclesFloatMessage("accx",x));
        bundle.add(oscTidalCyclesFloatMessage("accy",y));
        bundle.add(oscTidalCyclesFloatMessage("accz",z));
        osc.send(bundle, tidalCyclesAddress);
      }
      break;
    case "/linearaccel":
      // BNO005 linear acceleration data
      println("Mapping BNO-005 linear accelerometer data (" + oscMessage.addrPattern() + " " + oscMessage.typetag() + ") to TidalCycles controller input"); 
      if(oscMessage.checkTypetag("iifff")) {
        // Message contains float values for x,y,z
        int bodyId = oscMessage.get(0).intValue();
        int wearableId = oscMessage.get(1).intValue();

        float x = oscMessage.get(2).floatValue();
        float y = oscMessage.get(3).floatValue();
        float z = oscMessage.get(4).floatValue();    
        println("Values: " + x + " | " + y + " | " + z);
        OscBundle bundle = new OscBundle();
        bundle.add(oscTidalCyclesFloatMessage("linaccx",x));
        bundle.add(oscTidalCyclesFloatMessage("linaccy",y));
        bundle.add(oscTidalCyclesFloatMessage("linaccz",z));
        osc.send(bundle, tidalCyclesAddress);
      }    
      break;


    default: 
      // Ignore message
      println("Ignoring unknown message (" + oscMessage.addrPattern() + " " + oscMessage.typetag() + ")");
  }
}
