/**
 * Instant Composition - OSC Mapper
 * VLIEGWERK // Niels Janssen 
 *  
 * TidalCycles OSC Controller Input (see https://tidalcycles.org/index.php/Controller_Input)
 */
const osc = require("osc"); 

// Create an osc.js UDP Port for sending messages to TidalCycles
const TIDAL_CYCLES_OSC_ADDRESS="/ctrl";
const TIDAL_CYCLES_HOST="127.0.0.1";
const TIDAL_CYCLES_PORT=6010;
const tidalCyclesPort = new osc.UDPPort({
    // This is where sclang is listening for OSC messages.
    localAddress: "0.0.0.0",
    remoteAddress: TIDAL_CYCLES_HOST,
    remotePort: TIDAL_CYCLES_PORT,
    broadcast: true,
    metadata: false
});

tidalCyclesPort.open();

// When the TidalCycles port is ready
tidalCyclesPort.on("ready", function () {
    console.log("TidalCycles UDP port " + TIDAL_CYCLES_PORT + " is ready for sending OSC bundles and messages");
});

// When an error is read from the TidalCycles port
tidalCyclesPort.on("error", function (error) {
    console.log("An error occurred on TidalCycles port: ", error.message);
});

module.exports.sendOscMessage = (message,type,value) => {
    console.log("+ sendOscTidalCycles: " + TIDAL_CYCLES_OSC_ADDRESS + " " + message + " " + value + " ("+ type + ")")
    const oscCtrlMessage = {
        address: TIDAL_CYCLES_OSC_ADDRESS,
        args: [
            {
                type: "s",
                value: message
            },
            {
                type: type,
                value: value
            }
        ]
    };
    tidalCyclesPort.send(oscCtrlMessage);      
}

