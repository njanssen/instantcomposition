/**
 * Instant Composition - OSC Mapper
 * VLIEGWERK // Niels Janssen 
 *  
 * Test script for sending TidalCycles OSC Controller Input
 */
const tidalcycles = require('../apps/tidalcycles')
const normalizer = require("../osc-normalize")

setTimeout(function() {
    testOscTidalCycles();
}, 3000);

function testOscTidalCycles() { 
    tidalcycles.sendOscMessage("test_x","f","0.1")
    tidalcycles.sendOscMessage("test_x_damped","f",normalizer.dampFloat(0.1))
}

