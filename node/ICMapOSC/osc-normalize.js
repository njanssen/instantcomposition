/**
 * Instant Composition - OSC Mapper
 * VLIEGWERK // Niels Janssen 
 *  
 * OSC Data normalization
 */

// Min-max normalization [-1,1]
module.exports.normalizeFloat = (input,min,max) => {
    average      = (min + max) / 2;
    range        = (max - min) / 2;
    normalized_x = (input - average) / range;
    return normalized_x;
} 

// tanh(x*3) value of normalized [-1,1] input
module.exports.dampFloat = (input,mul = 3) => {
    x = input * mul;
    damped_x = Math.tanh(x);
    return damped_x;
}