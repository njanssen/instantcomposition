/**
* Instant Composition - OSC Mapper
 * VLIEGWERK // Niels Janssen 
 *  
 * OSC Mapping implementation
 * 
 * - Supported sensors
 *   + ICWrist
 * 
 * - Supported apps
 *   + TidalCycles OSC Controller Input (see https://tidalcycles.org/index.php/Controller_Input)
 */
const tidalcycles = require('./apps/tidalcycles')
const normalizer = require("./osc-normalize")

module.exports.mapOscBundle = (oscBundle, timeTag) => {
    // Nothing to do
}

module.exports.mapOscMessage = (oscMessage, timeTag) => {
    // Read signature
    const address = oscMessage.address
    const args = oscMessage.args
    const type = getOscTypeTag(oscMessage)

    let oscDeviceId = "unknown"
    if (type.startsWith('s')) {
        oscDeviceId = args[0].value.toLowerCase()
    }

    switch(address) {
        case "/ic/linearaccel":
            // BNO005 linear acceleration data
            // console.log("mapOscMessage: mapping BNO-005 linear accelerometer data (" + address + " " + type + ") to TidalCycles controller input"); 
            if(type === "sfff") {                
                // Message contains float values for x,y,z
                //tidalcycles.sendOscMessage(oscDeviceId + "_linearaccel_x",args[1].type,args[1].value)
                //tidalcycles.sendOscMessage(oscDeviceId + "_linearaccel_y",args[2].type,args[2].value)
                //tidalcycles.sendOscMessage(oscDeviceId + "_linearaccel_z",args[3].type,args[3].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_linearaccel_x",args[1].type,normalizer.dampFloat(args[1].value))
                tidalcycles.sendOscMessage(oscDeviceId + "_linearaccel_y",args[2].type,normalizer.dampFloat(args[2].value))
                tidalcycles.sendOscMessage(oscDeviceId + "_linearaccel_z",args[3].type,normalizer.dampFloat(args[3].value))
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;
        case "/ic/quaternion":
            // BNO005 quaternion data
            // console.log("mapOscMessage: mapping BNO-005 quaternion data (" + address + " " + type + ") to TidalCycles controller input"); 
            if(type === "sffff") {
                // Message contains float values for w,x,y,z
                tidalcycles.sendOscMessage(oscDeviceId + "_quaternion_w",args[1].type,args[1].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_quaternion_x",args[2].type,args[2].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_quaternion_y",args[3].type,args[3].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_quaternion_z",args[4].type,args[4].value)
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;
        case "/ic/accel":
            // BNO005 acceleration data
            // console.log("mapOscMessage: mapping BNO-005 accelerometer data (" + address + " " + type + ") to TidalCycles controller input"); 
            if(type === "sfff") {
                // Message contains float values for x,y,z
                //tidalcycles.sendOscMessage(oscDeviceId + "_accel_x",args[1].type,args[1].value)
                //tidalcycles.sendOscMessage(oscDeviceId + "_accel_y",args[2].type,args[2].value)
                //tidalcycles.sendOscMessage(oscDeviceId + "_accel_z",args[3].type,args[3].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_accel_x",args[1].type,normalizer.dampFloat(args[1].value))
                tidalcycles.sendOscMessage(oscDeviceId + "_accel_y",args[2].type,normalizer.dampFloat(args[2].value))
                tidalcycles.sendOscMessage(oscDeviceId + "_accel_z",args[3].type,normalizer.dampFloat(args[3].value))
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;
        case "/ic/gyro":
            // BNO005 gyrometer data
            // console.log("mapOscMessage: mapping BNO-005 gyrometer data (" + address + " " + type + ") to TidalCycles controller input"); 
            if(type === "sfff") {
                // Message contains float values for x,y,z
                tidalcycles.sendOscMessage(oscDeviceId + "_gyro_x",args[1].type,args[1].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_gyro_y",args[2].type,args[2].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_gyro_z",args[3].type,args[3].value)
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;
        case "/ic/magnetometer":
            // BNO005 acceleration data
            // console.log("mapOscMessage: mapping BNO-005 magnetometer data (" + address + " " + type + ") to TidalCycles controller input"); 
            if(type === "sfff") {
                // Message contains float values for x,y,z
                tidalcycles.sendOscMessage(oscDeviceId + "_magnetometer_x",args[1].type,args[1].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_magnetometer_y",args[2].type,args[2].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_magnetometer_z",args[3].type,args[3].value)
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;
        case "/ic/euler":      
            // BNO005 orientation data
            if(type === "sfff") {
                // Message contains float values for x,y,z
                tidalcycles.sendOscMessage(oscDeviceId + "_euler_x",args[1].type,args[1].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_euler_y",args[2].type,args[2].value)
                tidalcycles.sendOscMessage(oscDeviceId + "_euler_z",args[3].type,args[3].value)
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;      
        case "/ic/ecg":
            // Heart rate data, raw value
            if(type === "si") {
                // Message contains float values for x,y,z
                tidalcycles.sendOscMessage(oscDeviceId + "_ecg_data",args[1].type,args[1].value)
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;                                
        case "/ic/ecg/bpm":
            // Heart rate data, calculated BPM
            if(type === "sf") {
                // Message contains float values for x,y,z
                tidalcycles.sendOscMessage(oscDeviceId + "_ecg_bpm",args[1].type,args[1].value)
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;                                
        case "/ic/ecg/qrs":
            // Heart rate data
            if(type === "si") {
                // Message contains int value 1 when heart hits QRS period
                tidalcycles.sendOscMessage(oscDeviceId + "_ecg_qrs",args[1].type,args[1].value)
            } else {
                console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
            }   
            break;                                
        case "/ic/breath":
                // Heart rate data
                if(type === "sf") {
                    // Message contains float values for breath
                    tidalcycles.sendOscMessage(oscDeviceId + "_breath",args[1].type,args[1].value)
                } else {
                    console.log("! skipping message " + address + ", it is incorrectly typed: " + type)
                }   
                break;                                                            
            case "/ic/calibration":
        case "/ic/temperature":
        case "/ic/vbat":
        default:
            // do nothing
            console.log("mapOscMessage: ignoring " + address + " (" + type + ")");
            break;
    }
}

function getOscTypeTag(oscMessage) {
    const args = oscMessage.args
    let type = "";    
    for (let i = 0, len = args.length; i < len; i++) {
        const arg = args[i];
        type += arg.type
    }    
    return type;
}
