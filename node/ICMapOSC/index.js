/**
* Instant Composition - OSC Mapper
 * VLIEGWERK // Niels Janssen 
 *  
 * OSC Mapper creates connection between IC sensors and apps
 * 
 * Usage: 
 * $ node index.js
 */
const listener = require('./osc-listener')
