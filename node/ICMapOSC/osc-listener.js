/**
 * Instant Composition - OSC Mapper
 * VLIEGWERK // Niels Janssen 
 *  
 * OSC Listener implementation
 */
const osc = require("osc"); 
const mapping = require("./osc-mapping"); 

// Create an osc.js UDP Port listening on port 9000.
const LISTEN_HOST="0.0.0.0";
const LISTEN_PORT=9000;
const listenPort = new osc.UDPPort({
    localAddress: LISTEN_HOST,
    localPort: LISTEN_PORT,
    metadata: true
});

// Open the UDP ports
listenPort.open();

// When the listen port is ready
listenPort.on("ready", function () {
    console.log("Now listening on UDP port " + LISTEN_PORT + " for OSC bundles and messages");
});

// When an error is read from the listen port
listenPort.on("error", function (error) {
    console.log("An error occurred on listen port: ", error.message);
});


// Listen for incoming OSC bundles.
listenPort.on("bundle", function (oscBundle, timeTag, info) {
    //console.log("An OSC bundle just arrived for time tag", timeTag, ":\n", oscBundle);
    //console.log("Remote info is: ", info);
    mapping.mapOscBundle(oscBundle,timeTag)
});

// Listen for incoming OSC messages.
listenPort.on("message", function (oscMessage, timeTag, info) {
    //console.log("An OSC message just arrived for time tag", timeTag, ":\n", oscMessage);
    //console.log("Remote info is: ", info);
    mapping.mapOscMessage(oscMessage,timeTag)
});