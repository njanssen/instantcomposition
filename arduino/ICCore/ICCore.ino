/**
   Instant Composition
   VLIEGWERK // Niels Janssen

   Wearable core sensor for dancer

   - Designed for
     + Adafruit Feather M0 WiFi
     + Adafruit BNO-005 9-DOF sensor
     + Sparkfun Single Lead Heart Rate Monitor (ECG) AD8232
     + Custom build breath sensor (measures chest expansion)
   - Broadcasts data as OSC messages /ic/* over UDP port 9000, first argument (String-typed) is the device ID
   - Heart rate analysis based on https://github.com/blakeMilner/real_time_QRS_detection
*/
#include <ICArduino.h>
#include "credentials.h"

#define OSC_DEVICE_ID "CORE1"

ICArduino IC(
  SECRET_SSID,
  SECRET_PASS,
  OSC_DEVICE_ID,
  IPAddress(255, 255, 255, 255),
  9000,
  9001
);

// Stretch sensor
#define SERIESRESISTOR 10000
#define STRETCHPIN A0
#define PULSEPIN A3

// Sparkfun Single Lead Heart Rate Monitor (ECG) AD8232
#define M 5
#define N 30
#define winSize 125 // 250  
#define HP_CONSTANT ((float)1 / (float)M)
#define MAX_BPM 190

// resolution of RNG
#define RAND_RES 100000000

// constants won't change. Used here to set a pin number
const int LED_PIN = 13; // the number of the LED pin (digital)
const int ECG_PIN = A2; // the number of the ECG pin (analog)
const int PWM_PIN = 1;  // the number of the PWM pin (analog)

// pins for leads off detection
const int LEADS_OFF_PLUS_PIN = 10;  // the number of the LO+ pin (digital)
const int LEADS_OFF_MINUS_PIN = 11; // the number of the LO- pin (digital)

// timing variables
unsigned long previousEcgMicros = 0;    // will store last time LED was updated
unsigned long currentEcgQrsMicros = 0;   // time at which last QRS was found
unsigned long previousEcgQrsMicros = 0; // time at which QRS before last was found
unsigned long currentMicros = 0;	   // current time
unsigned long previousImuMicros = 0;
unsigned long previousBreathMicros = 0;
unsigned long previousPulseMicros = 0;

boolean sendOscQrs = false;

#define LED_ON_TIME_MICROS 200000

// interval at which to take samples and iterate algorithm (microseconds)
const long ECG_SAMPLERATE_DELAY_MS = 1000000 / winSize;
const long BREATH_SAMPLERATE_DELAY_MS = 50;
const long PULSE_SAMPLERATE_DELAY_MS = 50;

// circular buffer for BPM averaging
float bpm = 0;

#define BPM_BUFFER_SIZE 5
unsigned long bpm_buff[BPM_BUFFER_SIZE] = {0};
int bpm_buff_WR_idx = 0;
int bpm_buff_RD_idx = 0;

int tmp = 0;

void setup()
{
  // set the digital pin as output:
  pinMode(LED_PIN, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(ECG_PIN, INPUT);

  // leads for electrodes off detection
  pinMode(LEADS_OFF_PLUS_PIN, INPUT);  // Setup for leads off detection LO +
  pinMode(LEADS_OFF_MINUS_PIN, INPUT); // Setup for leads off detection LO -

  // set LED to off
  digitalWrite(LED_PIN, LOW);

  // Initialize serial
  Serial.begin(9600);

  if (!IC.begin() || !IC.enableImu()) {
    Serial.println("Problem occured when initializing WiFi networking and sensors");
    // don't continue:
    while (true)
      ;
  }

  IC.enableProfiling();
}

void loop()
{
  // Ensure that we have a network connection
  while (!IC.isConnected())
  {
    // Connect to WPA/WPA2 network.
    Serial.println("loop: (re)connecting WiFi");
    IC.connect();
  }

  // Construct a bundle that contains all sensor data
  OSCBundle bundle;
  addOscSensorData(bundle);

  // Send sensor event data over OSC when it contains OSC messages
  if (bundle.size() > 0) {
    IC.sendOscBundle(bundle);
  }
}

void addOscSensorData(OSCBundle &bundle)
{
  Serial.println("addOscSensorData: begin");

//  // Check if it's time for new ECG data
//  currentMicros = micros();
//  if (currentMicros - previousEcgMicros >= ECG_SAMPLERATE_DELAY_MS) {
//
//    // Save the time we last sent out ECG data
//    previousEcgMicros = currentMicros;
//    IC.profileStepInit("addOscEcgData(...)");
//    addOscEcgData(bundle);
//    IC.profileStepReady();
//  } 

  // Check if it's time for new breath data
  currentMicros = micros();
  if (currentMicros - previousBreathMicros >= BREATH_SAMPLERATE_DELAY_MS) {

    // Save the time we last sent out ECG data
    previousBreathMicros = currentMicros;
    IC.profileStepInit("addOscBreathData(...)");
    addOscBreathData(bundle);
    IC.profileStepReady();
  } 

  // Check if it's time for new breath data
  currentMicros = micros();
  if (currentMicros - previousPulseMicros >= PULSE_SAMPLERATE_DELAY_MS) {

    // Save the time we last sent out ECG data
    previousPulseMicros = currentMicros;
    IC.profileStepInit("addOscPulseData(...)");
    addOscPulseData(bundle);
    IC.profileStepReady();
  } 

  // Check if it's time for new IMU data
  currentMicros = micros();
  if (currentMicros - previousImuMicros >= IMU_SAMPLERATE_DELAY_MS)
  {
    // Save the time we last sent out IMU data
    previousImuMicros = currentMicros;

    // Add IMU sensor data to the OSC bundle
    IC.addOscImuData(bundle);
  }
 
  //IC.addOscFeatherData(bundle);
  
}

void addOscBreathData(OSCBundle &bundle)
{
  /* Stretch */
  float reading;

  reading = analogRead(STRETCHPIN);

  // convert the value to resistance
  reading = (1023 / reading) - 1;    // (1023/ADC - 1)
  reading = SERIESRESISTOR / reading; // 10K / (1023/ADC - 1)
  // Stretch cord resistance
  //Serial.println(reading); // TODO: send via OSC

  bundle.add("/ic/breath")
  .add(OSC_DEVICE_ID)
  .add(reading);
}

void addOscPulseData(OSCBundle &bundle)
{
  /* Stretch */
  float reading;

  reading = analogRead(PULSEPIN);

//  // convert the value to resistance
//  reading = (1023 / reading) - 1;    // (1023/ADC - 1)
//  reading = SERIESRESISTOR / reading; // 10K / (1023/ADC - 1)
//  // Stretch cord resistance
//  //Serial.println(reading); // TODO: send via OSC

  Serial.print("Pulse ");
  Serial.println(reading);

  bundle.add("/ic/pulse")
  .add(OSC_DEVICE_ID)
  .add(reading);
}


void addOscEcgData(OSCBundle &bundle)
{
  // see if it's time to turn LED off
  if ((currentMicros - currentEcgQrsMicros) > LED_ON_TIME_MICROS)
  {
    digitalWrite(LED_PIN, LOW);
    sendOscQrs = false;
  }

  // only read in data and perform detection if leads are on
  boolean qrsDetected = false;

  // only read data if ECG chip has detected that leads are attached to the dancer
  boolean leadsAreOn = (digitalRead(LEADS_OFF_PLUS_PIN) == 0) && (digitalRead(LEADS_OFF_MINUS_PIN) == 0);

  bundle.add("/ic/ecg/connected")
  .add(OSC_DEVICE_ID)
  .add(leadsAreOn);

  if (leadsAreOn)
  {
    // read next ECG data point
    int ecgDataPoint = analogRead(ECG_PIN);

    bundle.add("/ic/ecg")
    .add(OSC_DEVICE_ID)
    .add((int)ecgDataPoint);

    // give next data point to algorithm
    qrsDetected = detectEcgQrs(ecgDataPoint);

    bundle.add("/ic/ecg/qrs")
    .add(OSC_DEVICE_ID)
    .add((int)(sendOscQrs ? 1 : 0));

    if (qrsDetected)
    {
      currentEcgQrsMicros = micros();
      sendOscQrs = true;

      // set the LED with the ledState of the variable:
      digitalWrite(LED_PIN, HIGH);

      // BPM calculations
      bpm_buff[bpm_buff_WR_idx] = (60.0 / (((float)(currentEcgQrsMicros - previousEcgQrsMicros)) / 1000000.0));
      bpm_buff_WR_idx++;
      bpm_buff_WR_idx %= BPM_BUFFER_SIZE;

      bpm += bpm_buff[bpm_buff_RD_idx];

      tmp = bpm_buff_RD_idx - BPM_BUFFER_SIZE + 1;
      if (tmp < 0)
        tmp += BPM_BUFFER_SIZE;

      bpm -= bpm_buff[tmp];

      bpm_buff_RD_idx++;
      bpm_buff_RD_idx %= BPM_BUFFER_SIZE;

      previousEcgQrsMicros = currentEcgQrsMicros;
     
      //int PWM_value = ((float)bpm / (float)MAX_BPM) * 255;
      //analogWrite(5, PWM_value);
    }

    Serial.print("****** BPM: ");
    Serial.println(bpm / ((float) BPM_BUFFER_SIZE - 1));
    
    // Send BPM value over OSC
    bundle.add("/ic/ecg/bpm")
    .add(OSC_DEVICE_ID)
    .add(bpm / ((float) BPM_BUFFER_SIZE - 1));

  }
}

/* Portion pertaining to Pan-Tompkins QRS detection */

// circular buffer for input ecg signal
// we need to keep a history of M + 1 samples for HP filter
float ecg_buff[M + 1] = {0};
int ecg_buff_WR_idx = 0;
int ecg_buff_RD_idx = 0;

// circular buffer for input ecg signal
// we need to keep a history of N+1 samples for LP filter
float hp_buff[N + 1] = {0};
int hp_buff_WR_idx = 0;
int hp_buff_RD_idx = 0;

// LP filter outputs a single point for every input point
// This goes straight to adaptive filtering for eval
float next_eval_pt = 0;

// running sums for HP and LP filters, values shifted in FILO
float hp_sum = 0;
float lp_sum = 0;

// working variables for adaptive thresholding
float treshold = 0;
boolean triggered = false;
int trig_time = 0;
float win_max = 0;
int win_idx = 0;

// numebr of starting iterations, used determine when moving windows are filled
int number_iter = 0;

boolean detectEcgQrs(float new_ecg_pt)
{
  // copy new point into circular buffer, increment index
  ecg_buff[ecg_buff_WR_idx++] = new_ecg_pt;
  ecg_buff_WR_idx %= (M + 1);

  /* High pass filtering */
  if (number_iter < M)
  {
    // first fill buffer with enough points for HP filter
    hp_sum += ecg_buff[ecg_buff_RD_idx];
    hp_buff[hp_buff_WR_idx] = 0;
  }
  else
  {
    hp_sum += ecg_buff[ecg_buff_RD_idx];

    tmp = ecg_buff_RD_idx - M;
    if (tmp < 0)
      tmp += M + 1;

    hp_sum -= ecg_buff[tmp];

    float y1 = 0;
    float y2 = 0;

    tmp = (ecg_buff_RD_idx - ((M + 1) / 2));
    if (tmp < 0)
      tmp += M + 1;

    y2 = ecg_buff[tmp];

    y1 = HP_CONSTANT * hp_sum;

    hp_buff[hp_buff_WR_idx] = y2 - y1;
  }

  // done reading ECG buffer, increment position
  ecg_buff_RD_idx++;
  ecg_buff_RD_idx %= (M + 1);

  // done writing to HP buffer, increment position
  hp_buff_WR_idx++;
  hp_buff_WR_idx %= (N + 1);

  /* Low pass filtering */

  // shift in new sample from high pass filter
  lp_sum += hp_buff[hp_buff_RD_idx] * hp_buff[hp_buff_RD_idx];

  if (number_iter < N)
  {
    // first fill buffer with enough points for LP filter
    next_eval_pt = 0;
  }
  else
  {
    // shift out oldest data point
    tmp = hp_buff_RD_idx - N;
    if (tmp < 0)
      tmp += (N + 1);

    lp_sum -= hp_buff[tmp] * hp_buff[tmp];

    next_eval_pt = lp_sum;
  }

  // done reading HP buffer, increment position
  hp_buff_RD_idx++;
  hp_buff_RD_idx %= (N + 1);

  /* Adapative thresholding beat detection */
  // set initial threshold
  if (number_iter < winSize)
  {
    if (next_eval_pt > treshold)
    {
      treshold = next_eval_pt;
    }

    // only increment number_iter iff it is less than winSize
    // if it is bigger, then the counter serves no further purpose
    number_iter++;
  }

  // check if detection hold off period has passed
  if (triggered == true)
  {
    trig_time++;

    if (trig_time >= 100)
    {
      triggered = false;
      trig_time = 0;
    }
  }

  // find if we have a new max
  if (next_eval_pt > win_max)
    win_max = next_eval_pt;

  // find if we are above adaptive threshold
  if (next_eval_pt > treshold && !triggered)
  {
    triggered = true;

    return true;
  }
  // else we'll finish the function before returning FALSE,
  // to potentially change threshold

  // adjust adaptive threshold using max of signal found
  // in previous window
  if (win_idx++ >= winSize)
  {
    // weighting factor for determining the contribution of
    // the current peak value to the threshold adjustment
    float gamma = 0.175;

    // forgetting factor -
    // rate at which we forget old observations
    // choose a random value between 0.01 and 0.1 for this,
    float alpha = 0.01 + (((float)random(0, RAND_RES) / (float)(RAND_RES)) * ((0.1 - 0.01)));

    // compute new threshold
    treshold = alpha * gamma * win_max + (1 - alpha) * treshold;

    // reset current window index
    win_idx = 0;
    win_max = -10000000;
  }

  // return false if we didn't detect a new QRS
  return false;
}
