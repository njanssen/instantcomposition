
/**
   Instant Composition
   VLIEGWERK // Niels Janssen

   Wearable wrist sensor for dancer

   - Designed for
     + Adafruit Feather M0 WiFi
     + Adafruit BNO-005 9-DOF sensor
   - Visualizes sensor data on Neopixels
   - Displays WIFI connection and BNO-005 calibration status on Neopixels
   - Broadcasts data as OSC messages /ic/wrist/* over UDP port 9000
*/

#include <ICArduino.h>
#include "credentials.h"

#define OSC_DEVICE_ID "WRIST1"

ICArduino IC(
  SECRET_SSID,
  SECRET_PASS,
  OSC_DEVICE_ID,
  IPAddress(255, 255, 255, 255),
  9000,
  9001
);

// Adafruit NeoPixel
#include <Adafruit_NeoPixel.h>
#define NEOPIXEL_PIN 6
Adafruit_NeoPixel strip = Adafruit_NeoPixel(32, NEOPIXEL_PIN, NEO_GRB + NEO_KHZ800);
#define NEOPIXEL_WIFI_LED 0
#define NEOPIXEL_BNO005_LED 8
#define NEOPIXEL_EXTRA_LED1 16
#define NEOPIXEL_EXTRA_LED2 24

// timing variables
unsigned long currentMicros = 0;
unsigned long previousImuMicros = 0;

void setup()
{
  // Initialize NeoPixels
  strip.begin();
  strip.setBrightness(50);
  strip.show(); // Initialize all pixels to 'off'

  // Initialize serial
  Serial.begin(9600);

  if (!IC.begin() || !IC.enableImu()) {
    Serial.println("Problem occured when initializing WiFi networking and sensors");
    // don't continue:
    while (true)
      ;
  }

  //IC.enableProfiling();
}

void loop()
{
  setNeoPixelWifiStatus();

  // Ensure that we have a network connection
  while (!IC.isConnected())
  {
    // Connect to WPA/WPA2 network.
    Serial.println("loop: (re)connecting WiFi");
    IC.connect();
  }

  // Construct a bundle that contains all sensor data
  OSCBundle bundle;
  addOscSensorData(bundle);

  // Send sensor event data over OSC when it contains OSC messages
  if (bundle.size() > 0) {
    IC.sendOscBundle(bundle);
  }
}

void addOscSensorData(OSCBundle &bundle)
{
  currentMicros = micros();

  // Check if it's time for new IMU data
  if (currentMicros - previousImuMicros >= IMU_SAMPLERATE_DELAY_MS)
  {
    Serial.println("addOscSensorData: new IMU data sample: begin");
    // Save the time we last sent out IMU data
    previousImuMicros = currentMicros;

    // Add IMU sensor data to the OSC bundle
    IC.addOscImuData(bundle);

    pixelizeImuData();
    //Display BNO-005 IMU calibration status
    //setNeoPixelImuCalibrationStatus();

    //Serial.println("addOscSensorData: new IMU data sample: end");
  }

  //IC.addOscFeatherData(bundle);

}

void pixelizeImuData()
{
  imu::Vector<3> accelerometer = IC.getImuLinearAccel();
  int brightnessR = min(max(abs((int)accelerometer.x()) * 8, 10), 255);
  int brightnessG = min(max(abs((int)accelerometer.y()) * 8, 10), 255);
  int brightnessB = min(max(abs((int)accelerometer.z()) * 8, 10), 255);
  setNeoPixelRGB(brightnessR, brightnessG, brightnessB);
}

void setNeoPixelRGB(int brightnessR, int brightnessG, int brightnessB)
{
  for (uint16_t i = 0; i < strip.numPixels(); i++)
  {
    if (!isNeoPixelStatusLED(i))
    {
      strip.setPixelColor(i, brightnessR, brightnessG, brightnessB);
      strip.show();
    }
  }
}

boolean isNeoPixelStatusLED(int i)
{
  if (i == NEOPIXEL_WIFI_LED || i == NEOPIXEL_BNO005_LED || i == NEOPIXEL_EXTRA_LED1 || i == NEOPIXEL_EXTRA_LED2)
  {
    return true;
  }
  return false;
}

void setNeoPixelWifiStatus()
{
  switch (IC.isConnected())
  {
    case true:
      strip.setPixelColor(NEOPIXEL_WIFI_LED, 0, 64, 0);
      break;
    default:
      strip.setPixelColor(NEOPIXEL_WIFI_LED, 64, 0, 0);
      break;
  }
  strip.show();
}

void setNeoPixelImuCalibrationStatus()
{
  // 1 - BNO-005 Calibratation (system) status [red = 0, orange = 1,2, green = 3]
  switch (IC.getImuCalibrationStatusSystem())
  {
    case 3:
      strip.setPixelColor(NEOPIXEL_BNO005_LED, 0, 64, 0);
      break;
    case 2:
      strip.setPixelColor(NEOPIXEL_BNO005_LED, 32, 64, 0);
      break;
    case 1:
      strip.setPixelColor(NEOPIXEL_BNO005_LED, 64, 32, 0);
      break;
    case 0:
    default:
      strip.setPixelColor(NEOPIXEL_BNO005_LED, 64, 0, 0);
      break;
  }
  strip.show();
}
