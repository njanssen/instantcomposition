/**
   Instant Composition
   VLIEGWERK // Niels Janssen

   Implementation of the Arduino library for wearable sensons
*/
#include <ICArduino.h>

ICArduino::ICArduino(String networkSSID, String networkPass, String oscDeviceId, IPAddress oscOutAddress, unsigned int oscOutPort, unsigned int oscInPort)
{
    _networkSSID = networkSSID;
    _networkPass = networkPass;
    _oscDeviceId = oscDeviceId;
    _oscOutAddress = oscOutAddress;
    _oscOutPort = oscOutPort;
    _oscInPort = oscInPort;
    _profilingEnabled = false;

    // Configure pins for Adafruit ATWINC1500 Feather
    WiFi.setPins(8, 7, 4, 2);

    // Initialize WiFi UDP
    WiFiUDP _udp;
}

boolean ICArduino::begin()
{
    // check for the presence of the shield:
    if (_networkStatus == WL_NO_SHIELD)
    {
        Serial.println("WiFi shield not present");
        return false;
    }

    return true;
}

void ICArduino::enableProfiling() {
    _profilingEnabled = true;
}

boolean ICArduino::getProfilingEnabled() {
    return _profilingEnabled;
}


boolean ICArduino::enableImu()
{
    _imuEnabled = true;
    _bno = Adafruit_BNO055(55);

    /* Initialise the sensor */
    if (!_bno.begin())
    {
        /* There was a problem detecting the BNO055 ... check your connections */
        Serial.print("No BNO055 detected ... Check your wiring or I2C ADDR!");
        // don't continue:
        return false;
    }

    delay(1000);

    /* Use external crystal for better accuracy */
    _bno.setExtCrystalUse(true);

    /* Display some basic information on this sensor */
    _printImuDetails();

    /* Display current sensor status */
    _printImuStatus();

    return true;
}

boolean ICArduino::connect()
{
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(_networkSSID);

    // Connect to WPA/WPA2 network.
    if (_networkPass.length() == 0 ) { 
        WiFi.begin(_networkSSID);
    } else {
        WiFi.begin(_networkSSID, _networkPass);
    }

    // wait 10 seconds for connection:
    delay(10000);

    boolean connected = isConnected();

    if (connected)
    {
        _printNetworkStatus();
        _udp.begin(_oscInPort);
    }

    return connected;
}

boolean ICArduino::isConnected()
{
    return (WiFi.status() == WL_CONNECTED);
}

void ICArduino::addOscImuData(OSCBundle &bundle)
{
    char oscDeviceId[_oscDeviceId.length()];
    _oscDeviceId.toCharArray(oscDeviceId, 50);

    /* Disabled calibration status, it's freezing up the Arduino
    _getImuCalibrationStatus();
    Serial.println("/ic/calibration");
    bundle.add("/ic/calibration")
        .add(oscDeviceId)
        .add(_system)
        .add(_gyro)
        .add(_accel)
        .add(_mag);
    */

    // x = heading - 0 - 360
    // y = roll - -90 to 90
    // z = pitch - -180 to 180
    profileStepInit("/ic/euler");
    imu::Vector<3> euler = getImuEuler();
    bundle.add("/ic/euler")
        .add(oscDeviceId)
        .add((float)euler.x())
        .add((float)euler.y())
        .add((float)euler.z());
    profileStepReady();        

    // Pitch +180 to -180
    // Roll -90 to 90
    // Heading/Yaw - 0 - 360
    // w
    // x
    // y
    // z
    profileStepInit("/ic/quaternion");
    imu::Quaternion quat = getImuQuat();
    bundle.add("/ic/quaternion")
        .add(oscDeviceId)
        .add((float)quat.w())
        .add((float)quat.x())
        .add((float)quat.y())
        .add((float)quat.z());
    profileStepReady();           

    // Sensor value [-40,40]
    profileStepInit("/ic/accel");
    imu::Vector<3> accelerometer = getImuAccel();
    bundle.add("/ic/accel")
        .add(oscDeviceId)
        //.add((float)accelerometer.x())
        //.add((float)accelerometer.y())
        //.add((float)accelerometer.z());
        .add((float)normalizeFloat(accelerometer.x(),-40,40))
        .add((float)normalizeFloat(accelerometer.y(),-40,40))
        .add((float)normalizeFloat(accelerometer.z(),-40,40));
    profileStepReady();        

    // Sensor value [-50,50]    
    profileStepInit("/ic/linearaccel");
    imu::Vector<3> linearaccel = getImuLinearAccel();
    bundle.add("/ic/linearaccel")
        .add(oscDeviceId)
        //.add((float)linearaccel.x())
        //.add((float)linearaccel.y())
        //.add((float)linearaccel.z());
        .add((float)normalizeFloat(linearaccel.x(),-50,50))
        .add((float)normalizeFloat(linearaccel.y(),-50,50))
        .add((float)normalizeFloat(linearaccel.z(),-50,50));
    profileStepReady();        

    profileStepInit("/ic/gyro");
    imu::Vector<3> gyroscope = getImuGyro();
    bundle.add("/ic/gyro")
        .add(oscDeviceId)
        .add((float)gyroscope.x())
        .add((float)gyroscope.y())
        .add((float)gyroscope.z());
    profileStepReady();        

    // Micro Tesla
    profileStepInit("/ic/magnetometer");
    imu::Vector<3> magnetometer = getImuMag();
    bundle.add("/ic/magnetometer")
        .add(oscDeviceId)
        .add((float)magnetometer.x())
        .add((float)magnetometer.y())
        .add((float)magnetometer.z());
    profileStepReady();        

    profileStepInit("/ic/temperature");
    bundle.add("/ic/temperature")
        .add(oscDeviceId)
        .add(getImuTemp());
    profileStepReady();        
}

void ICArduino::addOscFeatherData(OSCBundle &bundle)
{
    char oscDeviceId[_oscDeviceId.length()];
    _oscDeviceId.toCharArray(oscDeviceId, 50);

    bundle.add("/ic/vbat")
        .add(oscDeviceId)
        .add(getFeatherMeasuredBat());
}

void ICArduino::sendOscBundle(OSCBundle bundle)
{
    profileStepInit("sendOscBundle(..)");
    _udp.beginPacket(_oscOutAddress, _oscOutPort);
    bundle.send(_udp);
    _udp.endPacket();
    profileStepReady();             
}

void ICArduino::sendOscMessage(OSCMessage message)
{
    profileStepInit("sendOscMessage(..)");
    _udp.beginPacket(_oscOutAddress, _oscOutPort);
    message.send(_udp);
    _udp.endPacket();
    profileStepReady();            
}

/* Normalization functions */
float ICArduino::normalizeFloat(float input, float min, float max) {
    float average = (min + max) / (float)2;
    float range = (max - min) / (float)2;
    float normalized_input = (input - average) / range;
    return normalized_input;
}

/* WiFi101 functions */
void ICArduino::_printNetworkStatus()
{
    // print the SSID of the network you're attached to:
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // print your WiFi shield's IP address:
    IPAddress ip = WiFi.localIP();
    Serial.print("IP Address: ");
    Serial.println(ip);

    // print the received signal strength:
    long rssi = WiFi.RSSI();
    Serial.print("signal strength (RSSI):");
    Serial.print(rssi);
    Serial.println(" dBm");
}

float ICArduino::getFeatherMeasuredBat()
{
    float measuredvbat = analogRead(FEATHER_VBAT_PIN);
    measuredvbat *= 2;    // we divided by 2, so multiply back
    measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
    measuredvbat /= 1024; // convert to voltage
    return (float)measuredvbat;
}

imu::Vector<3> ICArduino::getImuEuler()
{
    return _bno.getVector(Adafruit_BNO055::VECTOR_EULER);
}

imu::Vector<3> ICArduino::getImuLinearAccel()
{
    return _bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
}

imu::Quaternion ICArduino::getImuQuat()
{
    return _bno.getQuat();
}

imu::Vector<3> ICArduino::getImuAccel()
{
    return _bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
}

imu::Vector<3> ICArduino::getImuGyro()
{
    return _bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
}

imu::Vector<3> ICArduino::getImuMag()
{
    return _bno.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);
}

int ICArduino::getImuTemp()
{
    return (int)_bno.getTemp();
}

uint8_t ICArduino::getImuCalibrationStatusSystem()
{
    //_getImuCalibrationStatus();
    return _system;
}

uint8_t ICArduino::getImuCalibrationStatusGyro()
{
    //_getImuCalibrationStatus();
    return _gyro;
}

uint8_t ICArduino::getImuCalibrationStatusMag()
{
    //_getImuCalibrationStatus();
    return _mag;
}

uint8_t ICArduino::getImuCalibrationStatusAccel()
{
    //_getImuCalibrationStatus();
    return _accel;
}

void ICArduino::_getImuCalibrationStatus()
{
    _system = _gyro = _accel = _mag = 0;
    _bno.getCalibration(&_system, &_gyro, &_accel, &_mag);
}

void ICArduino::_printImuDetails()
{
    sensor_t sensor;
    _bno.getSensor(&sensor);
    Serial.println("------------------------------------");
    Serial.print("Sensor:       ");
    Serial.println(sensor.name);
    Serial.print("Driver Ver:   ");
    Serial.println(sensor.version);
    Serial.print("Unique ID:    ");
    Serial.println(sensor.sensor_id);
    Serial.print("Max Value:    ");
    Serial.print(sensor.max_value);
    Serial.println(" xxx");
    Serial.print("Min Value:    ");
    Serial.print(sensor.min_value);
    Serial.println(" xxx");
    Serial.print("Resolution:   ");
    Serial.print(sensor.resolution);
    Serial.println(" xxx");
    Serial.println("------------------------------------");
    Serial.println("");
    delay(500);
}

void ICArduino::_printImuStatus()
{
    /* Get the system status values (mostly for debugging purposes) */
    uint8_t system_status, self_test_results, system_error;
    system_status = self_test_results = system_error = 0;
    _bno.getSystemStatus(&system_status, &self_test_results, &system_error);

    /* Display the results in the Serial Monitor */
    Serial.println("");
    Serial.print("System Status: 0x");
    Serial.println(system_status, HEX);
    Serial.print("Self Test:     0x");
    Serial.println(self_test_results, HEX);
    Serial.print("System Error:  0x");
    Serial.println(system_error, HEX);
    Serial.println("");
    delay(500);
}

void ICArduino::_printImuCalibrationStatus()
{
    /* Get the four calibration values (0..3) */
    /* Any sensor data reporting 0 should be ignored, */
    /* 3 means 'fully calibrated" */
    uint8_t system, gyro, accel, mag;
    system = gyro = accel = mag = 0;
    _bno.getCalibration(&system, &gyro, &accel, &mag);

    /* The data should be ignored until the system calibration is > 0 */
    Serial.print("\t");
    if (!system)
    {
        Serial.print("! ");
    }

    /* Display the individual values */
    Serial.print("Sys:");
    Serial.print(system, DEC);
    Serial.print(" G:");
    Serial.print(gyro, DEC);
    Serial.print(" A:");
    Serial.print(accel, DEC);
    Serial.print(" M:");
    Serial.print(mag, DEC);
}

void ICArduino::profileStepInit(String step) { 
    if (_profilingEnabled) {         
        _currentStep = step;            
        Serial.print("*** STEP: ");
        Serial.print(_currentStep);
        Serial.println(" [init]");
        _previousStepMillis = millis();   
    }
}

void ICArduino::profileStepReady() { 
    if (_profilingEnabled) { 
        unsigned long delta = millis() - _previousStepMillis;
        Serial.print("*** STEP: ");
        Serial.print(_currentStep);
        Serial.print(" [ready] : ");
        Serial.print(delta);        
        Serial.println(" ms");
    }
}
