/**
   Instant Composition
   VLIEGWERK // Niels Janssen

   Arduino library for wearable sensons

   - Designed for
     + Adafruit Feather M0 WiFi
     + Adafruit BNO-005 9-DOF IMU sensor
     + Sparkfun Single Lead Heart Rate Monitor (ECG) AD8232
     + Custom build breath sensor (measures chest expansion)
   - Broadcasts data as OSC messages /ic/* over UDP port 9000, first argument (String-typed) is the device ID
   - Heart rate analysis based on https://github.com/blakeMilner/real_time_QRS_detection
   
*/
#ifndef ICArduino_h
#define ICArduino_h

#include "Arduino.h"

// OSC library from https://github.com/CNMAT/OSC
#include <OSCMessage.h>
#include <OSCBundle.h>

#include <WiFi101.h>
#include <WiFiUdp.h>

// BNO-055 library for Bosch 9-DOF IMU sensor
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>
#define IMU_SAMPLERATE_DELAY_MS (10) // Set the delay between fresh samples

// Adafruit Feather M0
#define FEATHER_VBAT_PIN A7

class ICArduino
{
  public:
    ICArduino(String networkSSID, String networkPass, String oscDeviceId, IPAddress oscOutAddress, unsigned int oscOutPort, unsigned int oscInPort);

    boolean begin();
    boolean connect();    
    boolean isConnected();
    boolean enableImu();
    void sendOscBundle(OSCBundle bundle);
    void sendOscMessage(OSCMessage message);

    // Profiling functions
    void enableProfiling();
    boolean getProfilingEnabled();
    void profileStepInit(String step);
    void profileStepReady();

    // Populate OSC bundle with sensor data
    void addOscImuData(OSCBundle &bundle);
    void addOscFeatherData(OSCBundle &bundle);

    // Adafruit Feather functions
    float getFeatherMeasuredBat();

    // Bosch BNO-005 IMU functions
    imu::Vector<3> getImuEuler();
    imu::Vector<3> getImuLinearAccel();
    imu::Quaternion getImuQuat();
    imu::Vector<3> getImuAccel();
    imu::Vector<3> getImuGyro();
    imu::Vector<3> getImuMag();
    int getImuTemp();
    uint8_t getImuCalibrationStatusSystem();
    uint8_t getImuCalibrationStatusGyro();
    uint8_t getImuCalibrationStatusMag();
    uint8_t getImuCalibrationStatusAccel();

    // Data functions
    float normalizeFloat(float input, float min, float max);

  private:
    String _networkSSID;
    String _networkPass;
    String _oscDeviceId;
    IPAddress _oscOutAddress;
    unsigned int _oscOutPort;
    unsigned int _oscInPort;
    int _networkStatus;    
    WiFiUDP _udp;       
    Adafruit_BNO055 _bno;
    boolean _imuEnabled;
    boolean _profilingEnabled;
     
    // calibration data
    uint8_t _system, _gyro, _accel, _mag;

    // profiling variables
    String _currentStep;    
    unsigned long _previousStepMillis;

    void _printNetworkStatus();
    void _printImuDetails();
    void _printImuStatus();
    void _printImuCalibrationStatus();
    void _getImuCalibrationStatus();
};

#endif